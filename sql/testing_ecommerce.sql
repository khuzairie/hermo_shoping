/*
Navicat MySQL Data Transfer

Source Server         : localAzwan
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : testing_ecommerce

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-09-21 17:18:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `brand`
-- ----------------------------
DROP TABLE IF EXISTS `brand`;
CREATE TABLE `brand` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_brand` varchar(50) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of brand
-- ----------------------------
INSERT INTO `brand` VALUES ('1', 'ASUS', null);
INSERT INTO `brand` VALUES ('2', 'NIKON', null);
INSERT INTO `brand` VALUES ('3', 'SAMSUNG', null);
INSERT INTO `brand` VALUES ('4', 'Dell', null);

-- ----------------------------
-- Table structure for `order_details`
-- ----------------------------
DROP TABLE IF EXISTS `order_details`;
CREATE TABLE `order_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `order_number` varchar(50) DEFAULT NULL,
  `unit_price` varchar(50) DEFAULT NULL,
  `quantity` varchar(10) DEFAULT NULL,
  `total_price` varchar(50) DEFAULT NULL,
  `promotion_code` varchar(40) DEFAULT NULL,
  `discaunt` varchar(40) DEFAULT NULL,
  `delivery_to` varchar(40) DEFAULT NULL,
  `shiping_fee` varchar(40) DEFAULT NULL,
  `grand_total` varchar(40) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `product_code` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of order_details
-- ----------------------------

-- ----------------------------
-- Table structure for `product`
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `brand` int(11) DEFAULT NULL,
  `selling_price` varchar(50) DEFAULT NULL,
  `retail_price` varchar(50) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `available_quantity` int(11) DEFAULT NULL,
  `image` longtext,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `product_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('1', 'Asus Ultra Notebook', '1', '100', '150', '15', '10', 'images/prv/product-3.jpg', null, 'a101');
INSERT INTO `product` VALUES ('2', 'Nikon X510', '2', '20', '25', '14', '10', 'images/prv/product-1.jpg', null, 'a102');
INSERT INTO `product` VALUES ('3', 'Samsung Galaxy', '3', '540', '500', '14', '10', 'images/prv/product-2.jpg', null, 'a103');
INSERT INTO `product` VALUES ('4', 'Mouse Optical', '4', '799', '700', '14', '10', 'images/prv/product-4.jpg', null, 'a104');
