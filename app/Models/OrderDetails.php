<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetails extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $table      = 'order_details';
    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];
}
