<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\updater;

class Product extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use updater;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table      = 'product';
    protected $primaryKey = 'id';
}
