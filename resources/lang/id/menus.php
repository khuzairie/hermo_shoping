<?php

return array (
  'language-picker' => 
  array (
    'language' => 'Bahasa',
    'langs' => 
    array (
      'en' => 'Inggris (English)',
      'id' => 'Bahasa Indonesia (Indonesian)',
    ),
  ),
);
