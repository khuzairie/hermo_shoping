<!DOCTYPE html>
<html>  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="index, follow">
    <title>Shopping Hermo Test</title>

    <!-- Essential styles -->

    <link href="{{ URL::asset('public/assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ URL::asset('public/font-awesome/css/font-awesome.min.css') }}" type="text/css"> 
    <link rel="stylesheet" href="{{ URL::asset('public/assets/fancybox/jquery.fancybox.css?v=2.1.5') }}" media="screen"> 

    <!-- Boomerang styles -->
    <link id="wpStylesheet" type="text/css" href="{{ URL::asset('public/css/global-style.css')}}" rel="stylesheet" media="screen">


    <!-- Favicon -->
    <link href="{{ URL::asset('public/images/favicon.png') }}" rel="icon" type="image/png">


    <!-- Assets -->
    <link rel="stylesheet" href="{{ URL::asset('public/assets/owl-carousel/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('public/assets/owl-carousel/owl.theme.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('public/assets/sky-forms/css/sky-forms.css')}}">    
    <!--[if lt IE 9]>
        <link rel="stylesheet" href="assets/sky-forms/css/sky-forms-ie8.css">
        <![endif]-->

        <!-- Required JS -->
        <script src="{{ URL::asset('public/js/jquery.js')}}"></script>
        <script src="{{ URL::asset('public/js/jquery-ui.min.js')}}"></script>

        <!-- Page scripts -->
        <link rel="stylesheet" href="{{ URL::asset('public/assets/ui-kit/css/jslider.css') }}">

    </head>
<!-- Body BEGIN -->
<body class="">
    <div class="body-wrap">

        <?php echo $content; ?>
    </div>

                <!-- MAIN CONTENT -->


            

            <!-- FOOTER -->

    </div>

    <!-- Essentials -->
    <script src="{{ URL::asset('public/js/modernizr.custom.js')}}"></script>
    <script src="{{ URL::asset('public/assets/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{ URL::asset('public/js/jquery.mousewheel-3.0.6.pack.js')}}"></script>
    <script src="{{ URL::asset('public/js/jquery.easing.js')}}"></script>
    <script src="{{ URL::asset('public/js/jquery.metadata.js')}}"></script>
    <script src="{{ URL::asset('public/js/jquery.hoverup.js')}}"></script>
    <script src="{{ URL::asset('public/js/jquery.hoverdir.js')}}"></script>
    <script src="{{ URL::asset('public/js/jquery.stellar.js')}}"></script>

    <!-- Boomerang mobile nav - Optional  -->
    <script src="{{ URL::asset('public/assets/responsive-mobile-nav/js/jquery.dlmenu.js')}}"></script>
    <script src="{{ URL::asset('public/assets/responsive-mobile-nav/js/jquery.dlmenu.autofill.js')}}"></script>

    <!-- Forms -->
    <script src="{{ URL::asset('public/assets/ui-kit/js/jquery.powerful-placeholder.min.js')}}"></script> 
    <script src="{{ URL::asset('public/assets/ui-kit/js/cusel.min.js')}}"></script>
    <script src="{{ URL::asset('public/assets/sky-forms/js/jquery.form.min.js')}}"></script>
    <script src="{{ URL::asset('public/assets/sky-forms/js/jquery.validate.min.js')}}"></script>
    <script src="{{ URL::asset('public/assets/sky-forms/js/jquery.maskedinput.min.js')}}"></script>
    <script src="{{ URL::asset('public/assets/sky-forms/js/jquery.modal.js')}}"></script>

    <!-- Assets -->
    <script src="{{ URL::asset('public/assets/hover-dropdown/bootstrap-hover-dropdown.min.js')}}"></script>
    <script src="{{ URL::asset('public/assets/page-scroller/jquery.ui.totop.min.js')}}"></script>
    <script src="{{ URL::asset('public/assets/mixitup/jquery.mixitup.js')}}"></script>
    <script src="{{ URL::asset('public/assets/mixitup/jquery.mixitup.init.js')}}"></script>
    <script src="{{ URL::asset('public/assets/fancybox/jquery.fancybox.pack.js?v=2.1.5')}}"></script>
    <script src="{{ URL::asset('public/assets/waypoints/waypoints.min.js')}}"></script>
    <script src="{{ URL::asset('public/assets/milestone-counter/jquery.countTo.js')}}"></script>
    <script src="{{ URL::asset('public/assets/easy-pie-chart/js/jquery.easypiechart.js')}}"></script>
    <script src="{{ URL::asset('public/assets/social-buttons/js/rrssb.min.js')}}"></script>
    <script src="{{ URL::asset('public/assets/nouislider/js/jquery.nouislider.min.js')}}"></script>
    <script src="{{ URL::asset('public/assets/owl-carousel/owl.carousel.js')}}"></script>
    <script src="{{ URL::asset('public/assets/bootstrap/js/tooltip.js')}}"></script>
    <script src="{{ URL::asset('public/assets/bootstrap/js/popover.js')}}"></script>

    <!-- Sripts for individual pages, depending on what plug-ins are used -->

    <!-- Boomerang App JS -->
    <script src="{{ URL::asset('public/js/wp.app.js')}}"></script>
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

    <!-- Temp -- You can remove this once you started to work on your project -->
    <script src="{{ URL::asset('public/js/jquery.cookie.js')}}"></script>
    <script src="{{ URL::asset('public/js/wp.switcher.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('public/js/wp.ga.js')}}"></script>
    <link href="{{ URL::asset('public/assets/css/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="{{ URL::asset('public/assets/js/sweetalert.min.js') }}"></script>
</body>
<!-- END BODY -->
</html>